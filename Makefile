build: clean Viewer.java
	javac Viewer.java

all: build javadoc

clean:
	rm -fr javadoc_html
	rm -fr *.class
	rm -fr ViewPlane/*.class

run: build
	java Viewer

javadoc:
	rm -fr javadoc_html
	mkdir javadoc_html
	javadoc -d javadoc_html *.java ViewPlane/*.java
