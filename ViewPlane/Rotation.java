package ViewPlane;

import java.awt.*;

/**
* Class that preforms the rotation of the objects.
*/
public class Rotation implements Runnable {

	/**
	* Double used for a precalculated value for theta.
	*/
	protected final static double theta = 5*Math.PI/180;
	
	/**
	* Another precalculated value for the SIN of theta
	*/
	protected final static double sin_theta = Math.sin(theta);

	/**
	* Another precalculated value for the COS of theta
	*/
	protected final static double cos_theta = Math.cos(theta);

	/**
	* Int to hold the speed of the rotation.
	*/
	public int speed;

	/**
	* Int used to repsent the axis. 
	*/
	public int axis;

	/**
	* Is the incoming viewplane onto which we project the image.
	*/
	protected ViewPlane v;

	/**
	* The container used for the window for the window manager.
	*/
	protected Container c;

	/**
	* View port used to view the viewplane through.
	*/
	protected ViewPort vp;

	/**
	* Constructor used for creating a Rotation object.
	* @param speed the incoming speed of the rotation.
	* @param v the viewplane that we are going to calculate with
	* @param c the container used to contain things.
	* @param vp the viewport is the viewport to view the figure.
	*/
	public Rotation(int speed, ViewPlane v, Container c, ViewPort vp) {
		this.speed = speed;
		this.v = v;
		this.c = c;
		this.vp = vp;
	}

	/**
	* Starts the rotation happening.
	*/
	public void run() {
		while(true) {

			try { Thread.sleep(100/speed); } 
			catch (InterruptedException e) {
				System.out.println("IT happened");
			}
			double x = v.norm.x;
			double y = v.norm.y;
			double z = v.norm.z;

			switch (axis) {
				case (1):
					v.norm.y = y*cos_theta - z*sin_theta;
					v.norm.z = z*cos_theta + y*sin_theta;
					break;
				case (2):
					v.norm.x = x*cos_theta + z*sin_theta;
					v.norm.z = z*cos_theta - x*sin_theta;
					break;
				case (3):
					v.norm.x = x*cos_theta - y*sin_theta;
					v.norm.y = x*sin_theta + y*cos_theta;
					break;
			}
			vp.repaint();
		}
    }

	/**
	* Sets the axis along which we want to rotate.
	*/
	public void set_axis(int axis) {
		this.axis = axis;
	}
}
