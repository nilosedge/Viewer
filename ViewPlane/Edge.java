package ViewPlane;

/**
* Class used for hold the infomation for one edge of the Model.
*/
public class Edge {

	/**
	*	Index of the First Vertex.
	*/
	public int v1;

	/**
	*	Index of the Second Vertex.
	*/
	public int v2;

	/**
	* Takes the vertex of the first and second index for edges.
	* @param vv1 this is the index of the first point on the edge.
	* @param vv2 this is the index of the second point on the edge.
	*/
	public Edge(int vv1, int vv2) {
		v1 = vv1; v2 = vv2;
	}
}
