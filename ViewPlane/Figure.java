//  Figure.java 
//	 A graphical application that allows the user to view wire frame
//	 images.

package ViewPlane;

import java.io.*;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.StringTokenizer;

/**
* Class used for reading number from the Model file.
*/
class NumberReader {

	/**
	* BufferedReader used to read in the Model file.
	*/
	protected BufferedReader fin;
	/**
	* Buffer for each line of text.
	*/
	protected String line;  //  Line of input text
	/**
	* Tokenizer used to tokenize the line read in from the file.
	*/
	protected StringTokenizer tokenizer;

	/**
	* Funciton used for reading in a file and tokenizing it.
	* @param reader is for taking in a buffered reader and using it to read files.
	*/
	public NumberReader(BufferedReader reader) {
		fin = reader;
		line = "";
		tokenizer = new StringTokenizer(line);
	}

	/**
	* Funciton used for reading one int value from the line.
	*/
	public int getInt() {
		int result = 0;
		try {
			while ( !tokenizer.hasMoreTokens() ) {
				line = fin.readLine();
				tokenizer = new StringTokenizer(line);
			}
			String token = tokenizer.nextToken();
			result = Integer.valueOf(token).intValue();
		} catch ( IOException e ) {
			System.out.println("I/0 exception---returning 0");
		} catch ( NumberFormatException e ) {
			System.out.println("Bad integer---returning 0");
		}
		return result;
	}
	/**
	* Funciton used for reading one double value from the line.
	*/
	public double getDouble() {
		double result = 0.0;
		try {
			while ( !tokenizer.hasMoreTokens() ) {
				line = fin.readLine();
				tokenizer = new StringTokenizer(line);
			}
			String token = tokenizer.nextToken();
			result = Double.parseDouble(token);
		} catch ( IOException e ) {
			System.out.println("I/0 exception---returning 0");
		} catch ( NumberFormatException e ) {
			System.out.println("Bad double---returning 0");
		}
		return result;
	}
	
	/**
	* Function used to get a string from the line.
	*/
	public String getString() {
		String result = "";
		try {
			while ( !tokenizer.hasMoreTokens() ) {
				line = fin.readLine();
				tokenizer = new StringTokenizer(line);
			}
			String token = tokenizer.nextToken();
			result = token;
		} catch ( IOException e ) {
			System.out.println("I/0 exception---returning 0");
		} catch ( NumberFormatException e ) {
			System.out.println("Bad string token---returning empty string");
		}
		return result;
	}
}


/**
* A set of world points and edges that define the wireframe object
*/
public class Figure {

	/**
	* An array of edges used to hold the connections to each point.
	*/
	public Edge[] edges;
	/**
	* An array of all the points used these never change.
	*/
	public WorldPoint[] verts;
	/**
	* An array of working points these point get transformed as the picture moves around.
	*/	
	public WorldPoint[] verts_working;
	/**
	* The count of all the edge.
	*/
	public int edge_count=0;
	/**
	* The could of all the verties used in the model.
	*/
	public int vert_count=0;

	/**
	* Function used to add a new Vertex takes 3 double values corispoding to x y z
	* @param x the distance along the x axis of the new vertex
	* @param y the distance along the y axis of the new vertex
	* @param z the distance along the z axis of the new vertex
	*/
	public void addVertex(double x, double y, double z) {
		verts[vert_count] = new WorldPoint(x, y, z);
		verts_working[vert_count++] = new WorldPoint(x, y, z);
	}

	/**
	* Function used for adding an edge to the edges array.
	* @param from the point that the edge starts.
	* @param to the point that the edge ends at.
	*/
	public void addEdge(int from, int to) {
		edges[edge_count++] = new Edge(from, to);
	}

	/**
	* Function used for loading in a new model.
	* @param filename is a string of the filename to load the new model from.
	*/
	public void loadFile(String filename) {
		edge_count=0;
		vert_count=0;
		//  Open the given file
		//  Read each line
		//  Parse the line, extracting ints or doubles where appropriate
		//  Create new points and edges arrays based on information in te
		//  file
		File file = new File("models/" + filename);
		if ( !file.exists() || !file.canRead() || file.isDirectory() ) {
			System.out.println("Cannot read " + file);
			return;
		}
		//  File ok, now try to read it
		try {
			FileReader fr = new FileReader(file);
			BufferedReader in = new BufferedReader(fr);
			NumberReader fin = new NumberReader(in);
			//  Get number of vertices
			int num_verts = fin.getInt();
			//System.out.println(num_verts + " verts to read");
			verts = new WorldPoint[num_verts];
			verts_working = new WorldPoint[num_verts];
			//  Then read all the vertices
			double x, y, z;
			for ( int i = 0; i < num_verts;  i++ ) {
				x = fin.getDouble();  
				y = fin.getDouble();  
				z = fin.getDouble();
				//System.out.println("(" + x + "," + y + "," + z + ")");
				addVertex(x, y, z);
			}
			//  Get number of edges
			int num_edges = fin.getInt();
 			//System.out.println(num_edges + " edges to read");
			edges = new Edge[num_edges];

			//  Then read all the edges
			int from, to;
			for ( int i = 0; i < num_edges;  i++ ) {
				from = fin.getInt();  
				to = fin.getInt();  
				//System.out.println("from: " + from + ",   to: " + to);
				addEdge(from, to);
				//System.out.println("from: " + from + ",   to: " + to);
			}

		} catch ( Exception e ) {
			System.out.println("**** Error occurred processing file");
		}
	}

	/**
	* Returns a reference to the newly created working verts.
	*/
	public WorldPoint[] getVertices() { 
		for(int i = 0; i < verts.length; i++) {
			verts_working[i] = new WorldPoint(verts[i]);
		} return verts_working; 
	}

	/**
	* Returns a reference to the edges array. 
	*/
	public Edge[] getEdges() { return edges; }

	/**
	* Returns the number of vertices in the figure.
	*/
	public int getNumberOfVertices() { return vert_count; }
	
	/**
	* Returns the number of edges in the figure. 
	*/
	public int getNumberOfEdges() { return edge_count; }

}

