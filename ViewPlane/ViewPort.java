package ViewPlane;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
* Class used for actully viewing a figure.
*/
public class ViewPort extends JPanel {

	/**
	* double used for holding the virtual Left point.
	*/
	protected double viewLeft;

	/**
	* double used for holding the virtual Top point.
	*/
	protected double viewTop;

	/**
	* double used for holding the virtual Width point.
	*/
	protected double viewWidth;

	/**
	* double used for holding the virtual Height point.
	*/
	protected double viewHeight;

	/**
	* double used for holding the real Left point.
	*/
	protected int realLeft;

	/**
	* double used for holding the real Top point.
	*/
	protected int realTop;

	/**
	* double used for holding the real Width point.
	*/
	protected int realWidth;

	/**
	* double used for holding the real Height point.
	*/
	protected int realHeight;

	/**
	* The actull figure object used for holding the model read in from a file.
	*/
	protected Figure figure;

	/**
	* Is the view information about where the used is looking at the figure.
	*/
	protected ViewPlane viewplane;

	/**
	* Is our working matrix for doing tranformations and such.
	*/
	protected Matrix mat;


	/**
	* ViewPort Creates all the objects used to display the figure.<BR><BR>
	* @param left is the Virtual X point of the window that shows up.<BR>
	* @param right is the Virtual Y point of the window that shows up.<BR>
	* @param width is the Virtual Width of the window that shows up.<BR>
	* @param height is the Virtual Height of the window that shows up.<BR>
	* @param rleft is the X value of the topleft point.<BR>
	* @param rtop is the Y value of the topleft point.<BR>
	* @param rwidth is the Width of the window that shows up.<BR> 
	* @param rheight is the Height of the window that shows up.<BR>
	* @param fig is a reference to the figure to be displayed.<BR>
	* @param view is a reference to the viewplane information.<BR>
	*/
	public ViewPort(
		double left,
		double top,
		double width,
		double height,
		int rleft,
		int rtop,
		int rwidth,
		int rheight,
		Figure fig,
		ViewPlane view) {

		//  Initialize the virtual extent of this viewport
		mat = new Matrix(true);
		viewLeft = left;	viewTop = top;
		viewWidth = width;  viewHeight = height;
		realLeft = rleft; realTop = rtop;
		realWidth = rwidth; realHeight = rheight;
		figure = fig;
		viewplane = view;
		setLayout(null);
		setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		setBackground(Color.white);
	}

	/**
	* Function actully points the figure to the screen.
	*/
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.blue);
		// g.translate((int)realWidth/2, (int)realHeight/2);

		mat.vpt(viewplane);

		Edge[] edges = figure.getEdges();
		int edge_count = figure.getNumberOfEdges();
		WorldPoint[] verts = figure.getVertices();
		int vert_count = figure.getNumberOfVertices();

		mat.transformVector(verts);

		for(int i = 0; i < edge_count; i++) {
			g.drawLine(
				(int)(0.5 + mapx_and_perspectx(verts[edges[i].v1].x, verts[edges[i].v1].z)),
				(int)(0.5 + mapy_and_perspecty(verts[edges[i].v1].y, verts[edges[i].v1].z)),
				(int)(0.5 + mapx_and_perspectx(verts[edges[i].v2].x, verts[edges[i].v2].z)),
				(int)(0.5 + mapy_and_perspecty(verts[edges[i].v2].y, verts[edges[i].v2].z))
			);
		}
	}

	/**
	* Maps the real point to the virtual world point, for the x value.
	* @param inx is the x value to be remaped to the virtual world.
	* @param z is the z value used to make the persceptive correct.
	*/
	public double mapx_and_perspectx(double inx, double z) {
		double D = (400/viewplane.D);
		inx = D * inx / (D + z);
		return ((((inx-viewLeft)/viewWidth)*(realWidth-realLeft)) + realLeft);
	}

	/**
	* Maps the real point to the virtual world point, for the y value.
	* @param iny is the y value to be remaped to the virtual world.
	* @param z is the z value used to make the persceptive correct.
	*/
	public double mapy_and_perspecty(double iny, double z) {
		double D = (400/viewplane.D);
		iny = D * iny / (D + z);
		return ((((iny-viewTop)/viewHeight)*(realHeight-realTop)) + realTop);
	}
}
