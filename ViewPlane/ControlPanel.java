//	The ControlPanel class allows the user to set viewplane paramters via
//	a dialog box

package ViewPlane;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

/**
* Class used to build a control panel for the z axis and all the rotations.
*/

public class ControlPanel extends JDialog {
	
	/**
	* JFrame that is the Parent window.
	*/
	JFrame parent;

	/**
	* ViewPlane that holds the infomation for looking through the viewport
	*/
	ViewPlane viewplane;

	/**
	* Figure Object for holding all the points of the Model
	*/
	Figure figure;

	/**
	* Panel for choosing the rotations
	*/
	JPanel rotationPanel;

	/**
	* Button used for loading in new figure files.
	*/
	JButton loadButton;

	/**
	* Slider used to control the z axis.
	*/
	JSlider zSlider;

	/**
	* Button used for cancaling the file loading.
	*/
	JButton quitButton;
	/**
	* ButtonGroup of all the roations buttons.
	*/
	ButtonGroup rotations;
	/**
	* Radio for choosing no rotation.
	*/
	JRadioButton noRotation;
	/**
	* Radio for choosing rotation around the X axis.
	*/
	JRadioButton xRotation;
	/**
	* Radio for choosing rotation around the Y axis.
	*/
	JRadioButton yRotation;
	/**
	* Radio for choosing rotation around the Z axis.
	*/
	JRadioButton zRotation;
	/**
	* Rotation object for rotating around a given axis.
	*/
	Rotation rot;
	/**
	* A Thread for rotations.
	*/
	Thread running;


	/**
	* An object for Controling all the operations of the Models.<BR><BR>
	* @param p is the frame the ControlPanel is going to connect to.<BR>
	* @param v is the ViewPlace used to View the Model<BR>
	* @param f is the Model<BR>
	* @param speed is the value of the rotation speed.<BR>
	* @param vp is the ViewPort that the ViewPlane uses to View the Model.<BR>
	*/
	public ControlPanel(JFrame p, ViewPlane v, Figure f, int speed, ViewPort vp) {

		super(p, "View Control Panel", false);
		parent = p;
		viewplane = v;
		figure = f;
		setSize(200, 300);
		Container contentPane = getContentPane();
		rot = new Rotation(speed, v, contentPane, vp);
		running = new Thread(rot);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		contentPane.setBackground(Color.black);

		JPanel zControl = new JPanel();
		zControl.setLayout(new BoxLayout(zControl, BoxLayout.Y_AXIS));
		zControl.setBorder(new TitledBorder("z"));
		zSlider = new JSlider(JSlider.VERTICAL, -100, 100, (int)viewplane.norm.z);
		zSlider.setPaintLabels(true);
		zSlider.setMajorTickSpacing(20);

		zSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				viewplane.norm.z = zSlider.getValue();
				getParent().repaint();
			}
		});

		zControl.add(zSlider);

		JPanel mainPanel = new JPanel();
		new BoxLayout(mainPanel, BoxLayout.Y_AXIS);

		mainPanel.add(zControl);

		rotationPanel = new JPanel();
		rotationPanel.setLayout(new BoxLayout(rotationPanel, BoxLayout.Y_AXIS));
		rotationPanel.setBorder(new TitledBorder("Rotation"));
 		rotations = new ButtonGroup();
		noRotation = new JRadioButton("None", true);

		noRotation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				running.stop();
			}
		});

		rotations.add(noRotation);

		xRotation = new JRadioButton("x axis", false);
		xRotation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rot.set_axis(1);
				if(!running.isAlive()) {
					running = new Thread(rot);
					running.start();
				}
			}
		});
		rotations.add(xRotation);

		yRotation = new JRadioButton("y axis", false);
		yRotation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rot.set_axis(2);
				if(!running.isAlive()) {
					running = new Thread(rot);
					running.start();
				}
 			}
		});
		rotations.add(yRotation);

		zRotation = new JRadioButton("z axis", false);
		zRotation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rot.set_axis(3);
				if(!running.isAlive()) {
					running = new Thread(rot);
					running.start();
				}
			}
		});

		rotations.add(zRotation);
		rotationPanel.add(noRotation);
		rotationPanel.add(xRotation);
		rotationPanel.add(yRotation);
		rotationPanel.add(zRotation);
		mainPanel.add(rotationPanel);
		contentPane.add(mainPanel);
		loadButton = new JButton("Load");

		loadButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser dialog = new JFileChooser(".");
				int choice = dialog.showOpenDialog(new Frame());
				if ( choice == JFileChooser.APPROVE_OPTION ) {
					figure.loadFile(dialog.getName(dialog.getSelectedFile()));
					parent.repaint();
				}
			}
		});

		quitButton = new JButton("Quit");
		quitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		JPanel buttonPanel = new JPanel(new FlowLayout());
		buttonPanel.add(loadButton);
		buttonPanel.add(quitButton);

		contentPane.add(buttonPanel);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent ev) {
				System.exit(0);
			}
		});

		setLocation(parent.getX() + parent.getWidth() + 10, 30);
	 	pack();
		setVisible(true);
	}
}
