//	Viewer.java 
//	 A graphical application that allows the user to view wire frame
//	 images.

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import ViewPlane.*;

/**
* Main class for creating a view for wireframe images.
*/
public class Viewer extends JFrame {

	/**
	* Virtual Left Value
	*/
	protected double virtualLeft;
	/**
	* Virtual Top Value
	*/
	protected double virtualTop;
	/**
	* Virtual Width Value
	*/
	protected double virtualWidth;
	/**
	* Virtual Height Value
	*/
	protected double virtualHeight;
	/**
	* ViewPort for Viewing the Model 
	*/
	protected ViewPort viewport;
	/**
	* Figure Object for holding all the points of the Model. 
	*/
	protected Figure figure;
	/**
	* ViewPlane that holds the infomation for looking through the viewport. 
	*/
	protected ViewPlane viewplaneInfo;
	/**
	* Rotation Speed of the rotations of the models.
	*/
	protected int rotationSpeed = 10;
	/**
	* Control Panel that pops up, controls the Z axis and all the rotations. 
	*/
	protected ControlPanel controlPanel;

	/**
	* Viewer Creates a window and controls that allow you to move around the model.<BR><BR>
	* @param realLeft is the X value of the topleft point.<BR>
	* @param realTop is the Y value of the topleft point.<BR>
	* @param realWidth is the Width of the window that shows up.<BR> 
	* @param realHeight is the Height of the window that shows up.<BR>
	* @param vLeft is the Virtual X point of the window that shows up.<BR>
	* @param vRight is the Virtual Y point of the window that shows up.<BR>
	* @param vWidth is the Virtual Width of the window that shows up.<BR>
	* @param vHeight is the Virtual Height of the window that shows up.<BR>
	*/

	public Viewer(int realLeft, int realTop, int realWidth, int realHeight, double vLeft, double vTop, double vWidth, double vHeight) {

		virtualLeft = vLeft;

		virtualTop = vTop;
		virtualWidth = vWidth;
		virtualHeight = vHeight;

		setTitle("Wire Frame Viewer");
		setLocation(realLeft, realTop);
		setSize(realWidth, realHeight);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		figure = new Figure();
		figure.loadFile("jet.vpt");

		viewplaneInfo = new ViewPlane(
			new WorldPoint(0, 0, 0),	// reference point
			new WorldPoint(0, 0, -10), // normal point
			new WorldPoint(0, 1, 0) // up point
		);

		viewport = new ViewPort(
			virtualLeft,
			virtualTop, 
			virtualWidth,
			virtualHeight,
			realLeft,
			realTop,
			realWidth,
			realHeight,
			figure,
			viewplaneInfo
		);

		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		cp.add(viewport, BorderLayout.CENTER);
		final JSlider xSlider = new JSlider(JSlider.HORIZONTAL, -100, 100, (int)viewplaneInfo.norm.x);
		xSlider.setPaintLabels(true);
		xSlider.setMajorTickSpacing(20);

		xSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				viewplaneInfo.norm.x = xSlider.getValue();
				repaint();
			}
		});

		cp.add(xSlider, BorderLayout.SOUTH);
		final JSlider ySlider = new JSlider(JSlider.VERTICAL, -100, 100, (int)viewplaneInfo.norm.y);
		ySlider.setPaintLabels(true);
		ySlider.setMajorTickSpacing(20);
		ySlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				viewplaneInfo.norm.y = ySlider.getValue();
				repaint();
			}
		});

		cp.add(ySlider, BorderLayout.EAST);
		setVisible(true);
		controlPanel = new ControlPanel(this, viewplaneInfo, figure, rotationSpeed, viewport);
	}

	public static void main(String[] args) {
		Viewer window = new Viewer(10, 10, 700, 700, -30.0, -30.0, 60.0, 60.0);
	}

}
